<img src="https://coursemarks.com/wp-content/uploads/2020/11/1695132_69b5_6.jpg">


### This will be a Restful X12 generator and parser. Using gin . 
### 270/271 Parser NOT FINISHED!!

##  libraries used so far
- [Golang Gin](https://github.com/gin-gonic/gin):github.com/gin-gonic/gin
- [GoLang Viper](https://github.com/spf13/viper):github.com/spf13/viper


# Strucuture
* Httpd
  * Handlers
    * X270ISAFormater.go
* main.go


- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] Build stringers for ISA Formatter
- [ ] Implement Swagger Documenation
- [ ] Clean up error catching and logging


# HETS 270/271 Search Options


| Search Option | MBI | Last Name | First Name | DOB|
|------|---------|---------|---------|---------|
| Primary | X | X | X | X |
| Alternate 1 |  X | X |
| Alternate 2 |  X | X | X |

# Deliminators 

|Character | Name | Delimiter |
|------|---------|---------|
| \* | Asterisk |Data Element Separator |
| \| | Pipe |Component Element Separator |
| ~ | Tilde | Segment Terminator |
| ^ | Carat | Repetition Separator |
 
Brief:

EDI (Electronic Data Interchange) is the transfer of data from one computer system to another by standardized message formatting, without the need for human intervention. EDI permits multiple companies -- possibly in different countries -- to exchange documents electronically.

EDI Health Care Eligibility/Benefit Inquiry (270) is used to inquire about the health care eligibility and benefits associated with a subscriber or dependent.
EDI Health Care Eligibility/Benefit Response (271) is used to respond to a request inquiry about the health care eligibility and benefits associated with a subscriber or dependent.
It's a class library developped in C# which creates a 270 request stream from raw information and also can convert back a 271 response in raw information which can be used to show to users as required.

Need:

Here is a sample 270 request stream.

```
ISA*00* *00* *12*ABCCOM *01*999999999 *120117*1719*U*00400*000006768*0*P*>
GS*HS*4405197800*999999999*20120117*1719*1421*X*004010VICS
ST*270*1234
BHT*0022*13*1*20010820*1330
HL*1**20*1
NM1*PR*2******PI*123456789
HL*2*1*21*1
NM1*1P*2******SV*987654321
HL*3*2*22*0
NM1*IL*1*DOE*JANE****MI*345678901
EQ*30**FAM
SE*10*1234
GE*1*1421
IEA*1*000006768
```
```
ISA*00*          *00*          *ZZ*1234567        *ZZ*11111          *170508*1141*^*00501*000000101*1*P*:~
GS*HC*XXXXXXX*XXXXX*20170617*1741*101*X*005010X279A1~
ST*270*1234*005010X279A1~
BHT*0022*13*10001234*20060501*1319~
HL*1**20*1~
NM1*PR*2*ABC COMPANY*****PI*842610001~
HL*2*1*21*1~
NM1*1P*2*BONE AND JOINT CLINIC*****SV*2000035~
HL*3*2*22*0~
TRN*1*93175-012547*9877281234~
NM1*IL*1*SMITH*ROBERT****MI*11122333301~
DMG*D8*19430519~
DTP*291*D8*20060501~
EQ*30~
SE*13*1234~
GE*1*101~
IEA*1*000000101~
```

To create this stream we need to go through a ASC X12 270/271 Comapnion Guide of more than 300 pages. Every segment, value and every loop in this block has a specific meaning. It is explained in the companion guide. There was a requirement where my webservice needed the data in JSON format.

How It Works:

We went through all the segments, values and loops mentioned in the 270/271 companion guide. As mentioned in the document we created different rules and created dictory for each segment and hard coded values for same. Also added logic to identify the information entered by the user and accordingly add in respective loop of the 270 stream.
For example, there are different loop and qualifiers for Subscriber and Dependent. A Special segment is there for sending additional information about the patient. There are different payer id given to each insurance company. Few fields are compulsory and few fields are optional. Also there are few values which varies on the basis of number of segments involved in the request stream.

Here are definitions for few segments.
ST - Transaction Set Header
SE - Transaction Set Trailer
ISA - Interchange Control Header
IEA - Interchange Control Trailer
GS - Functional Group Header
GE - Functional Group Trailer
HL - Information Source Level
NM1 - Information Receiver Name
BHT - Beginning of Hierachical Transaction

This library was talking data in EDI and JSON format and on the basis of predefined rules it was converting the request in to X12 270 EDI / JSON format.

Similarly it was also used to convert the 271 response.
#### -->End