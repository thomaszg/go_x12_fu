package main

import (
	"fmt"
	"goserver/httpd/handler"
	"log"

	"github.com/spf13/viper"

	"github.com/gin-gonic/gin"
)

var (
	defaults = map[string]interface{}{
		"senderid":        "0000001",
		"receiverid":      "0000000",
		"interexctlno":    "000000001",
		"port":            "8080",
		"host":            "0.0.0.0",
		"prouctionstatus": "T",
	}
	configName  = "config"
	configPaths = []string{
		".",
	}
	configType = "yaml"
)

//Config is struct for yaml values
type Config struct {
	Senderid         string
	ReceiverID       string
	InterexCtl       string
	Port             string
	Host             string
	Productoinstatus string
}

//Main Consider using Viper package to set global env, eg.subscriberIDs, reciverid
func main() {
	// Load default settings,
	for k, v := range defaults {
		viper.SetDefault(k, v)
	}
	viper.SetConfigName(configName)
	for _, p := range configPaths {
		viper.AddConfigPath(p)
	}
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("could not read config file: %v", err)
	}

	var config Config
	err = viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("could not decode econfig into structure %v", err)
	}
	fmt.Printf("Running on host: %s\n", config.Host)
	fmt.Printf("Running on Port: %s\n", config.Port)
	fmt.Printf("Starting at Senderid: %s\n", config.Senderid)
	fmt.Printf("Using Receiver ID: %s\n", config.ReceiverID)
	fmt.Printf("Using Interchange Control number: %s\n", config.InterexCtl)
	//->End Config

	r := gin.Default()
	// Just a test route
	r.GET("/ping", handler.PingGet)

	// Query string parameters are parsed using the existing underlying request object.
	// The request responds to a url matching:  /welcome?firstname=Jane&lastname=Doe
	r.GET("/welcome", handler.X270ISAFormatter)

	r.Run(":" + config.Port) // listen and ser 192.168.1.131/ve on 0.0.0.0:8080 (for windows "localhost:8080")

}
