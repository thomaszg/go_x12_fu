package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

//EOBRequest stuct will hold the json or post request, maybe add some validation once this works
type EOBRequest struct {
	SubmitterID            string //Unique Submitter ID provided by e.g CMS
	TransactionID          string `form:"transaction_id" json:"transaction_id"`                   //Unique Transaction ID for each request
	DateOfService          string `form:"date_of_service" json:"date_of_service"`                 // Subscriber/Patient First name
	SubscriberFname        string `form:"subscriber_fname" json:"subscriber_fname"`               // Subscriber/Patient middle name
	SubscriberLname        string `form:"subscriber_lname" json:"subscriber_lname"`               // Subscriber/Patient middle name
	SubscriberDOB          string `form:"subscriber_dob" json:"subscriber_dob"`                   // Subscriber/Patient DOB  YYYYMMDD
	SubscriberPolicyNumber string `form:"subscriber_policynumber" json:"subscriber_policynumber"` // Subscriber/Patient Policy Number
	SubscriberGender       string `form:"subscriber_gender" json:"subscriber_gender"`             // Subscriber/Patient Gender M/F/U
	Interdate              string `form:"interdate" json:"interdate"`                             // Interchange Date  YYYYMMDD
	Providernpi            string `form:"provider_npi" json:"provider_npi"`                       // Doctor NPI
	X12senderid            string `form:"x12sender_id" json:"x12sender_id"`                       // Interchange SenderID provdied by CMS
	Payercode              string `form:"payer_code" json:"payer_code"`                           // Insurance Payer Code
	X12version             string `form:"x12version" json:"x12version"`
	Payername              string `form:"payer_name" json:"payer_name"` // Insurance Payer Name
}

//x12Delimiters Preferred 270 Request Delimiters
type x12Delimiters struct {
	DataSeparator       string
	ComponentSeparator  string
	SegmentSeparator    string
	RepetitionSeparator string
}

//FullISA consider moving my map to a struct for formatting
type FullISA struct {
	ISA00 string // Segment ID control header
	ISA01 string // Authorization Information	Qualifier :‘00’ (zero, zero) – No Authorization	Information Present	(No meaningful information in ISA02)
	ISA02 string // Authorization Information: Blank fill with Blank 10Spaces
	ISA03 string // Security Information	Qualifier : ‘00’ (zero, zero) – No Authorization Information Present (No meaningful information in	ISA04)
	ISA04 string // Security Information: Blank fill with 10 Blank spaces
	ISA05 string // Interchagne ID Qualifier : 'ZZ'(for mutually defined)
	ISA06 string // Interchange ID 9 digit RRE number assinged by COB, this field must be 15 bytes in length, leading 0's should be used to populate teh 9 digits. The remaining 6-bytes should be filled with spaces.
	ISA07 string // Interchange ID Qualifier: 'ZZ'(for mutually defined)
	ISA08 string // Inerchange Receiver ID : 'CMS' - Field must be 15 bytes and left justified Fill balance of field with spaces [ Assigned by CMS or blueCross]
	ISA09 string // Interchange Date: Interchange Creation Date in YYMMDD format eg. 210130
	ISA10 string // Interchange Time : Interchange Create TIme in HHMM format e.g 1730
	ISA11 string // Interchagne Control Repetition Separator : '|'  (for U.S. EDI community of ASC X12, TDCC adn UCS)
	ISA12 string // Interchange Control Version Number: '005011'
	ISA13 string // Interchange Control Number: Unique number that should start with 1 and increment by 1 with each ISA record submitted. The number shoudl be 9 digits e.g 0000001, 000002 track this
	ISA14 string // Acknowledgement Requested : '0' (zero for non Interchange Acknowledgement Request) [Look into this Ack might be nice]
	ISA15 string //Usage Indicator : 'P' (for production Data) * Data set name determines whether file will be processed as production or test Always use 'P'
	ISA16 string // Component Element Separator : A ':' (colon) must be sent in this field

}

//FullGS consider moving my map to a struct for formatting Functional Group Header
type FullGS struct {
	GS00 string // Segment ID control header
	GS01 string //
	GS02 string // Application Sender’s Code: HETS always expects the Trading Partner,, Submitter ID assigned by CMS.
	GS03 string // Application Receiver’s Code, HETS always expects “CMS.”
	GS04 string // Date Functional Group Creation date in CCYYMMDD format eg 20090428
	GS05 string // Time, Functional Group Creation time in HHMM or HHMMSS format eg 1425 or 142530
	GS06 string // Group Control Number Unique number within the interchange that must be identical to value in GE02 (should begin with ‘1’ and increment by 1 for each	GS-GE) eg 01
	GS07 string //Responsible Agency Code: ‘X’ (from Accredited Standards Committee	X12) e.g X
	GS08 string // Version/Release/Industry Identifier Code ‘004010X092A1’ eg 004010X092A1, 005010X279A1

}

//FullBHT consider moving my map to a struct for formatting Beginning of Hierarchical Transaction – First segment of the 270 Transaction set
type FullBHT struct {
	BHT00 string // Beginning of Hierarchical Transactionr
	BHT01 string //
	BHT02 string // Transaction Set Purpose Code, expected 13
	BHT04 string // Transaction Set Creation Date Creation date of file expressed in CCYYMMDD format  e.g 20090428
	BHT05 string // Transaction Set Creation Time: Creation time of file expressed in format HHMM e.g 1411

}

//FullST Segment struct Transaction Set Header – Indicates the start of the Transaction Set
type FullST struct {
	ST00 string // Segment identifier
	ST01 string // Transaction Set Identifier ‘270’ (for Eligibility, Coverage, or Benefit Inquiry) 270
	ST02 string // Transaction Set Control Number Unique number to the interchange that must be identical to value in SE02 (should begin with ‘0001’ and increment by 1 for each STSE).  e.g 0001
}

//FullHL Segment struct
type FullHL struct {
	HL00 string // Segment identifier
	HL01 string // Hierarchical ID Number : HL01 must begin with the number one (1)	and increase by 1 for each subsequent HL segment. Only numeric values are allowed in HL01. e.g HL*1
	HL02 string // Used in loops 2.3....
	HL03 string // Hierarchical Level code : ‘20’ (for Information Source). e.g HL*1**20
	HL04 string // Hierarchical Child code ‘1’ (to indicate that subordinate HL segments will follow). e.g. HL*1**20*1
}

//FullNM1 Segment struct; Information Receiver Name
type FullNM1 struct {
	NM100 string // Segment Identifier
	NM101 string // Entity Identifier Code :‘PR’ (for payer) e.g. PR
	NM102 string // Entity Type Qualifier : ‘2’ (for Non-Person Entity) eg. 2
	NM103 string // First 6 bytes of Last Name of subscribe
	NM104 string //	 First initial of subscriber (insured)
	NM108 string // Identification Code Qualifier : ‘PI’ (for Payer Information)  e.g. PI
	NM109 string //Information Source Primary Identifier : ‘CMS’ e.g CMS
}

//HL2000A s Segment struct https://www.cms.gov/Medicare/Coordination-of-Benefits-and-Recovery/Mandatory-Insurer-Reporting-For-Non-Group-Health-Plans/Archive/Downloads/NGHPInterfaceSpecVersion21.pdf
//Please see function for loop details
type HL2000A struct {
	Loop int     // Loop count
	HL   FullHL  // Entity Identifier Code :‘PR’ (for payer) e.g. PR
	NM   FullNM1 // Entity Type Qualifier : ‘2’ (for Non-Person Entity) eg. 2
}

//FullREF Information Receiver Additional Identification
type FullREF struct {
	REF01 string // Reference Identification Qualifier ‘IG’ (for insurance policy number)  e.g IG
	REF02 string // Subscriber Supplemental	Identifier Plan enrollees Social Security Number (9-digits) Do not include hyphens.
}

//DMG The 2100C-DMG-Subscriber Demographic Information segment is situational. This segment should be sent when the value in
//2100C-NM109 is not known and the birth date can be used to identify the subscriber
type DMG struct {
	DMG00 string // segment identifier
	DMG01 string //Date Time Period Format D8’ (for Date Expressed in format CCYYMMDD)
	DMG02 string //Subscriber Birth Date Member’s Date of Birth in format CCYYMMDD
	DMG03 string //Subscriber Gender Code ‘F’ (for female) ‘M’ (for male) ‘U’ (for unknown)
}

//FullSE Transaction Set Trailer – Indicates the end of the Transaction Set
type FullSE struct {
	SE00 string // Seg identifier
	SE01 string // Transaction Segment Count Total Number of segments included in a	transaction set (including the ST and SE segments) e.g 42
	SE02 string // Transaction Set Control Number Unique number to the interchange that must be identical to the value in ST02. e.g 001
}

//FullGE  Functional Group Trailer
type FullGE struct {
	GE00 string // SEG Id
	GE01 string // Number of Transaction Sets Included : Total Number of transaction sets included in the functional group e.g 1
	GE02 string //Unique number assigned by the sender that	must be identical to GS06 e.g 0001
}

//FullEIA Interchange Control Trailer
type FullEIA struct {
	EIA00 string // seg id
	EIA01 string //Number of Included  Functional Groups : Count of the number of functional groups included in an interchange. e.g 1
	EIA02 string // Interchange Control Number : Control number assigned by the interchange sender that should be 9 characters and be identical to the value in ISA13. e.g 000000001
}

// X270ISAFormatter is a consumer of x270 data
// That will create an x1227050010
func X270ISAFormatter(c *gin.Context) {

	var eOBRequest EOBRequest
	// If `GET`, only `Form` binding engine (`query`) used.
	// If `POST`, first checks the `content-type` for `JSON` or `XML`, then uses `Form` (`form-data`).
	// See more at https://github.com/gin-gonic/gin/blob/master/binding/binding.go#L48
	if c.ShouldBind(&eOBRequest) == nil {
		log.Println(eOBRequest.TransactionID)
	}

	eOBRequest.TransactionID = c.DefaultQuery("transaction_id", "000001")               //Unique Transaction ID for each request
	eOBRequest.DateOfService = c.DefaultQuery("dateofservice", "20171028")              // Subscriber/Patient First name
	eOBRequest.SubscriberFname = c.DefaultQuery("subscriberfname", "**")                // Subscriber/Patient middle name
	eOBRequest.SubscriberDOB = c.DefaultQuery("subscriberdob", "")                      // Subscriber/Patient DOB  YYYYMMDD
	eOBRequest.SubscriberPolicyNumber = c.DefaultQuery("subscriberpolicynumber", "***") // Subscriber/Patient Policy Number
	eOBRequest.SubscriberGender = c.DefaultQuery("subscribergender", "U")               // Subscriber/Patient Gender M/F/U
	eOBRequest.Interdate = c.DefaultQuery("interdate", "20171028")                      // Interchange Date  YYYYMMDD
	eOBRequest.Providernpi = c.DefaultQuery("providernpi", "098765432")                 // Doctor NPI
	eOBRequest.X12senderid = c.DefaultQuery("x12senderid", "012345678")                 // Interchange SenderID provdied by CMS
	eOBRequest.Payercode = c.DefaultQuery("payercode", "***")                           // Insurance Payer Code
	eOBRequest.X12version = c.DefaultQuery("x12version", "005010X279A1")
	eOBRequest.Payername = c.DefaultQuery("payername", "****") // Insurance Payer Name

	CreateISA(eOBRequest)       // Generate Base ISA Portion of 270
	CreateGS(eOBRequest)        // Generate Base GS Portion of 270
	CreateST(eOBRequest)        // Generate Base ST
	CreateBHT(eOBRequest)       // Generate Base BHT
	CreateHL2000(eOBRequest, 1) //Generate Loop 1 2000A
	CreateHL2000(eOBRequest, 2) //Generate Loop 1 2000B
	CreateHL2000(eOBRequest, 3) //Generate Loop 1 2000C
	CreateREF(eOBRequest)       // Generate REF
	CreateDMG(eOBRequest)       //Generate DMG
	CreateSE(eOBRequest)        //Generate SE
	CreateGE(eOBRequest)

	//    c.String(http.statusOk, " %v", xmap['ISA00'])
	c.String(http.StatusOK, "Hello %s ", eOBRequest.TransactionID)

}

/**************** SEGMENT FUNCTION START *********************/

/* ISA Segment  - EDI-270 format */

/*CreateISA Generates ISA Portion of the 270
ISA05/ISA07
01 means you're using your duns number.
zz means you're using something mutually defined.
12 means telephone number.
14 means duns number with a suffix (if i remember correctly).
 Patient’s First Name (NM104)
 Patient’s Last Name (NM103)
 Patient’s Date of Birth (DMG02)
 Subscriber ID Number exactly as it appears on the Anthem ID card including alpha prefi x, if
 applicable (NM109)
 Dates of Eligibility requested by Provider (DTP03)
*/
//CreateISA
func CreateISA(eob EOBRequest) {

	t := time.Now()
	ISA := FullISA{
		ISA00: "ISA",                                              // Segment ID control header
		ISA01: "00",                                               // Authorization Information	Qualifier :‘00’ (zero, zero) – No Authorization	Information Present	(No meaningful information in ISA02)
		ISA02: "          ",                                       // Authorization Information: Blank fill with Blank 10Spaces
		ISA03: "00",                                               // Security Information	Qualifier : ‘00’ (zero, zero) – No Authorization Information Present (No meaningful information in	ISA04)
		ISA04: "          ",                                       // Security Information: Blank fill with 10 Blank spaces
		ISA05: "ZZ",                                               // Interchagne ID Qualifier : 'ZZ'(for mutually defined)
		ISA06: rightPad(leftPad2Len("012345678", "0", 9), " ", 6), // Interchange ID 9 digit RRE number assinged by COB, this field must be 15 bytes in length, leading 0's should be used to populate teh 9 digits. The remaining 6-bytes should be filled with spaces.
		ISA07: "ZZ",                                               // Interchange ID Qualifier: 'ZZ'(for mutually defined)
		ISA08: rightPad2Len("CMS", " ", 15),                       // Inerchange Receiver ID : 'CMS' - Field must be 15 bytes and left justified Fill balance of field with spaces [ Assigned by CMS or blueCross]
		ISA09: t.Format("060102"),                                 // Interchange Date: Interchange Creation Date in YYMMDD format eg. 210130
		ISA10: t.Format("1204"),                                   // Interchange Time : Interchange Create TIme in HHMM format e.g 1730
		ISA11: "|",                                                // Interchagne Control Repetition Separator : '|'  (for U.S. EDI community of ASC X12, TDCC adn UCS)
		ISA12: "005010",                                           // Interchange Control Version Number: '005010'
		ISA13: leftPad2Len(eob.TransactionID, "0", 9),             // Interchange Control Number: Unique number that should start with 1 and increment by 1 with each ISA record submitted. The number shoudl be 9 digits e.g 0000001, 000002 track this
		ISA14: "0",                                                // Acknowledgement Requested : '0' (zero for non Interchange Acknowledgement Request) [Look into this Ack might be nice]
		ISA15: "P",                                                //Usage Indicator : 'P' (for production Data) * Data set name determines whether file will be processed as production or test Always use 'P'
		ISA16: ":",                                                // Component Element Separator : A ':' (colon) must be sent in this field
	}
	//fmt.Println("My Struct:" + ISAJson(ISA))
	fmt.Println(ISA)

}

//ISAJson returns Json formated ISA value
//This prob needs to return and error as well
func ISAJson(ISA FullISA) string {
	ISAs, err := json.Marshal(ISA)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(ISAs)

}

//ISAString returns String formated
//This prob needs to return and error as well
func ISAString(ISA FullISA) string {
	ISAs, err := json.Marshal(ISA)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(ISAs)

}

//Stringer create formated GS Segment string
//Todo change segment to varibable
func (i FullISA) String() string {
	return fmt.Sprintf("%v*%v*%v*%v*%v*%v*%v*%v*%v*%v*%v*%v*%v*%v*%v*%v*%v*%v*%v~", i.ISA00, i.ISA01, i.ISA02, i.ISA03, i.ISA04, i.ISA05, i.ISA06, i.ISA07, i.ISA08, i.ISA09, i.ISA10, i.ISA11, i.ISA12, i.ISA12, i.ISA13, i.ISA14, i.ISA14, i.ISA15, i.ISA16)
}

//CreateGS GS segment formatter
func CreateGS(eob EOBRequest) {
	t := time.Now()
	GS := FullGS{

		GS01: "GS",                 //Segment ID control header
		GS02: eob.SubmitterID,      // Application Sender’s Code: HETS always expects the Trading Partner,, Submitter ID assigned by CMS.
		GS03: "CMS",                // Application Receiver’s Code, HETS always expects “CMS.”
		GS04: t.Format("20060102"), // Date Functional Group Creation date in CCYYMMDD format eg 20090428
		GS05: t.Format("1204"),     // Time, Functional Group Creation time in HHMM or HHMMSS format eg 1425 or 142530
		GS06: "",                   // Group Control Number Unique number within the interchange that must be identical to value in GE02 (should begin with ‘1’ and increment by 1 for each	GS-GE) eg 01
		GS07: "X",                  //Responsible Agency Code: ‘X’ (from Accredited Standards Committee	X12) e.g X
		GS08: "005010X279A1",       // Version/Release/Industry Identifier Code ‘004010X092A1’ eg 004010X092A1, 005010X279A1                                           // Component Element Separator : A ':' (colon) must be sent in this field
	}
	fmt.Println(GS)

}

//GSJson returns Json formated ISA value
//This prob needs to return and error as well
func GSJson(GS FullGS) string {
	GSs, err := json.Marshal(GS)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(GSs)

}

//GSString returns String formated
//This prob needs to return and error as well
func GSString(GS FullGS) string {
	GSs, err := json.Marshal(GS)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(GSs)

}

//String create formated ISA Segment string
//Todo change segment to varibable
func (i FullGS) String() string {
	return fmt.Sprintf("%v*%v*%v*%v*%v*%v*%v*%v~", i.GS01, i.GS02, i.GS03, i.GS04, i.GS05, i.GS06, i.GS07, i.GS08)
}

//CreateST ST segment formatter
func CreateST(eob EOBRequest) {
	ST := FullST{
		ST00: "ST",              // Segment identifier
		ST01: "270",             // Transaction Set Identifier ‘270’ (for Eligibility, Coverage, or Benefit Inquiry) 270
		ST02: eob.TransactionID, // Transaction Set Control Number Unique number to the interchange that must be identical to value in SE02 (should begin with ‘0001’ and increment by 1 for each STSE).  e.g 0001
	}
	fmt.Println(ST)
}

//STJson returns String formated
//This prob needs to return and error as well
func STJson(ST FullST) string {
	STj, err := json.Marshal(ST)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(STj)

}

//String create formated ISA Segment string
//Todo change segment to varibable
func (i FullST) String() string {
	return fmt.Sprintf("%v*%v*%v~", i.ST00, i.ST01, i.ST02)
}

//CreateBHT ST segment formatter
func CreateBHT(eob EOBRequest) {
	t := time.Now()
	BHT := FullBHT{
		BHT00: "BHT", // Beginning of Hierarchical Transactionr
		//BHT01: "",                   //
		BHT02: "13",                 // Transaction Set Purpose Code, expected 13
		BHT04: t.Format("20060102"), // Transaction Set Creation Date Creation date of file expressed in CCYYMMDD format  e.g 20090428
		BHT05: t.Format("1204"),     // Transaction Set Creation Time: Creation time of file expressed in format HHMM e.g 1411
	}
	fmt.Println(BHT)
}

//String create formated ISA Segment string
//Todo change segment to varibable
func (i FullBHT) String() string {
	return fmt.Sprintf("%v*%v*%v*%v~", i.BHT00, i.BHT02, i.BHT04, i.BHT05)
}

//BHTJson returns String formated
//This prob needs to return and error as well
func BHTJson(BHT FullBHT) string {
	BHTj, err := json.Marshal(BHT)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(BHTj)

}

//CreateHL2000 ST segment formatter
func CreateHL2000(eob EOBRequest, Loop int) {
	HL := FullHL{HL00: "HL"}
	NM1 := FullNM1{NM100: "NM1"}
	if Loop == 1 {
		HL = FullHL{
			HL00: "HL",
			HL01: "1",  // Hierarchical ID Number : HL01 must begin with the number one (1)	and increase by 1 for each subsequent HL segment. Only numeric values are allowed in HL01. e.g HL*1
			HL03: "20", // Hierarchical Level code : ‘20’ (for Information Source). e.g HL*1**20
			HL04: "1",  // Hierarchical Child code ‘1’ (to indicate that subordinate HL segments will follow). e.g. HL*1**20*1
		}

		//FullNM1 Segment struct; Information Receiver Name
		NM1 = FullNM1{
			NM100: "NM1", //
			NM101: "PR",  // Entity Identifier Code :‘PR’ (for payer) e.g. PR
			NM102: "2",   // Entity Type Qualifier : ‘2’ (for Non-Person Entity) eg. 2
			NM108: "PI",  // Identification Code Qualifier : ‘PI’ (for Payer Information)  e.g. PI
			NM109: "CMS", //Information Source Primary Identifier : ‘CMS’ e.g CMS
		}
	} else if Loop == 2 {

		//fmt.Println("Starting Loop 2")
		HL = FullHL{
			HL00: "HL",
			HL01: "2",  // HL01 value at this loop should be ‘2’. Only numeric values are allowed in HL01. e.g HL*2
			HL02: "1",  // Should always be ‘1’ at this loop
			HL03: "21", // Hierarchical Level code : ‘21’ (for Information Receiver) e.g. HL*2*1*21
			HL04: "1",  // ‘1’ (for Additional Subordinate HL Data Segment in this Hierarchical Structure –	refers to Subscriber info in 2000C loop) e.g. HL*2*1*21*1
		}
		//FullNM1 Segment struct; Information Receiver Name
		NM1 = FullNM1{
			NM100: "NM1",                           //
			NM101: "P5",                            // ‘P5’ (for Plan Sponsor)
			NM102: "2",                             // Entity Type Qualifier : ‘2’ (for Non-Person Entity) eg. 2
			NM108: "PI",                            // Identification Code Qualifier : ‘PI’ (for Payer Information)  e.g. PI
			NM109: leftPad2Len("12345678", "0", 9), // Information Receiver Identification Number  : The RRE number must contain 9 digits. Populate leading positions with zeros e.g.
		}
	} else if Loop == 3 {
		//fmt.Println("Starting Loop 3")
		HL = FullHL{
			HL00: "HL", //
			HL01: "3",  // HL01 value at this loop should begin with ‘3’ and increment by one for each new transaction in the transaction set. Only numeric values are allowed in HL01.
			HL02: "2",  //Should always be ‘2’ at this loop
			HL03: "22", // ‘22’ (for Subscriber)
			HL04: "0",  // ‘0’ (for No Subordinate HL Segment in this Structure Loop
		}
		//FullNM1 Segment struct; Information Receiver Name
		NM1 = FullNM1{
			NM100: "NM1",                              //
			NM101: "IL",                               // ‘IL’ (for insured or subscriber)
			NM102: "2",                                // ‘1’ (for Person)
			NM103: "",                                 // First 6 bytes of Last Name of subscribe
			NM104: "",                                 // First Initial of First Name
			NM108: "",                                 // ‘MI’ (for member identification number
			NM109: leftPad2Len("123456789A", "0", 10), // Subscriber Primary Member’s HICN e.g 123456789A
		}
	}

	HL2k := HL2000A{
		Loop: 1,   // Beginning of Hierarchical Transactionr
		HL:   HL,  // HL Section from above
		NM:   NM1, //HL section from above
	}
	fmt.Println(HL2k)
}

//HL2000Json returns String formated
//This prob needs to return and error as well
func HL2000Json(HL2k HL2000A) string {
	HL2kj, err := json.Marshal(HL2k)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(HL2kj)

}

//String create formated ISA Segment string
//Todo change segment to varibable
func (i HL2000A) String() string {
	return fmt.Sprintf("%v*%v*%v*%v~\n%v*%v*%v*%v*%v~", i.HL.HL00, i.HL.HL01, i.HL.HL03, i.HL.HL04, i.NM.NM100, i.NM.NM101, i.NM.NM102, i.NM.NM108, i.NM.NM109)
}

//CreateREF REF segment formatter
func CreateREF(eob EOBRequest) {
	BHT := FullREF{
		REF01: "IG", // Reference Identification Qualifier ‘IG’ (for insurance policy number)  e.g IG
		REF02: "",   // Subscriber Supplemental	Identifier Plan enrollees Social Security Number (9-digits) Do not include hyphens.
	}

	fmt.Println(BHT)
}

//Todo change segment to varibable
func (i FullREF) String() string {
	return fmt.Sprintf("%v*%v~", i.REF01, i.REF02)
}

//REFJson returns String formated
//This prob needs to return and error as well
func REFJson(REF FullREF) string {
	REFj, err := json.Marshal(REF)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(REFj)

}

//CreateDMG REF segment formatter
func CreateDMG(eob EOBRequest) {
	DMG := DMG{
		DMG00: "DMG",
		DMG01: "",                   //Date Time Period Format D8’ (for Date Expressed in format CCYYMMDD)
		DMG02: eob.SubscriberDOB,    //Subscriber Birth Date Member’s Date of Birth in format CCYYMMDD
		DMG03: eob.SubscriberGender, //Subscriber Gender Code ‘F’ (for female) ‘M’ (for male) ‘U’ (for unknown)
	}

	fmt.Println(DMG)
}

//Todo change segment to varibable
func (i DMG) String() string {
	return fmt.Sprintf("%v*%v*%v~", i.DMG00, i.DMG01, i.DMG03)
}

//DMGJson returns json formated string
//This prob needs to return and error as well
func DMGJson(REF DMG) string {
	DMGj, err := json.Marshal(REF)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(DMGj)

}

//CreateSE SE segment formatter
func CreateSE(eob EOBRequest) {
	SE := FullSE{
		SE00: "SE",              // Seg identifier
		SE01: "",                // Transaction Segment Count Total Number of segments included in a	transaction set (including the ST and SE segments) e.g 42
		SE02: eob.TransactionID, // Transaction Set Control Number Unique number to the interchange that must be identical to the value in ST02. e.g 001
	}
	fmt.Println(SE)
}

//Todo change segment to varibable
func (i FullSE) String() string {
	return fmt.Sprintf("%v*%v*%v~", i.SE00, i.SE01, i.SE02)
}

//SEJson returns json formated string
//This prob needs to return and error as well
func SEJson(SE FullSE) string {
	SEj, err := json.Marshal(SE)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(SEj)

}

//CreateGE GE segment formatter
func CreateGE(eob EOBRequest) {
	GE := FullGE{
		GE00: "GE", // SEG Id
		GE01: "",   // Number of Transaction Sets Included : Total Number of transaction sets included in the functional group e.g 1
		GE02: "",   //Unique number assigned by the sender that	must be identical to GS06 e.g 0001
	}
	fmt.Println(GE)
}

//Todo change segment to varibable
func (i FullGE) String() string {
	return fmt.Sprintf("%v*%v*%v~", i.GE00, i.GE01, i.GE02)
}

//GEJson returns json formated string
//This prob needs to return and error as well
func GEJson(GE FullGE) string {
	GEj, err := json.Marshal(GE)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(GEj)

}

//CreateEIA  segment formatter
func CreateEIA(eob EOBRequest) {
	EIA := FullEIA{
		EIA00: "EIA", // seg id
		EIA01: "",    //Number of Included  Functional Groups : Count of the number of functional groups included in an interchange. e.g 1
		EIA02: "",    // Interchange Control Number : Control number assigned by the interchange sender that should be 9 characters and be identical to the value in ISA13. e.g 000000001
	}
	fmt.Println(EIA)
}

//Todo change segment to varibable
func (i FullEIA) String() string {
	return fmt.Sprintf("%v*%v*%v~", i.EIA00, i.EIA01, i.EIA02)
}

//EIAson returns json formated string
//This prob needs to return and error as well
func EIAson(EIA FullEIA) string {
	EIAj, err := json.Marshal(EIA)
	if err != nil {
		fmt.Println(err)
		//Probalye need to return and error
	}
	return string(EIAj)

}
