package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// PingGet is ...a test route
func PingGet(c *gin.Context) {
	c.JSON(http.StatusOK, map[string]string{
		"Hello": "Found ME",
	})
}
